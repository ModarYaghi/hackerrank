"""Hacker Rank Problem Solving - Challenge 47; Hurdle Race"""


def hurdle_race(k, height):
    dose = max(height) - k
    return dose if dose > 0 else 0


# Primary tests' instances:
k, height = 1, [1, 2, 3, 3, 2]
print(hurdle_race(k, height))
# 3 - 1 = 2

k, height = 4, [1, 6, 3, 5, 2]
print(hurdle_race(k, height))
# 6 - 4 = 2

k, height = 7, [2, 5, 4, 5, 2]
print(hurdle_race(k, height))
# 0
