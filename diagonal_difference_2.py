"""
Hacker Rank Problem Solving - Challenge 05; Diagonal Difference;
One Line Solution Using List Comprehension
"""


def diagonal_difference(arr):

    return abs(sum([arr[i][i] - arr[i][len(arr) - 1 - i] for i in range(len(arr))]))


# The primary test instances:
arr = [[1, 2, 3], [4, 5, 6], [9, 8, 9]]
print(diagonal_difference(arr))
# 15 - 17 = 2

arr = [[11, 2, 4], [4, 5, 6], [10, 8, -12]]
print(diagonal_difference(arr))
# 4 - 19 = 15

# For testing 4x4 matrix:
arr = [
    [1, 2, 3, 4],
    [10, 20, 30, 40],
    [100, 200, 300, 400],
    [1000, 2000, 3000, 4000],
]
print(diagonal_difference(arr))
# |1234 - 4321| = 3087
