"""Hacker Rank Problem Solving - Challenge 12; Apple and Orange"""


def count_apples_and_oranges(s, t, a, b, apples, oranges):
    # s: int, starting point of Sam's house location.
    # t: int, ending point of Sam's house location.
    # a: int, Apple tree location
    # b: int, Orange tree location
    # apples: array[int], distances at which each apple falls from the tree.
    # oranges: array[int], distances at which each apple falls from the tree.

    apples_on_house = len([1 for i in apples if s <= i + a <= t])
    oranges_on_house = len([1 for i in oranges if s <= i + b <= t])

    # The challenge tests accept printed output only.
    print(apples_on_house)
    print(oranges_on_house)


# Primary tests' instances:
s, t, a, b, apples, oranges = 7, 10, 4, 12, [2, 3, -4], [3, -2, -4]
print(count_apples_and_oranges(s, t, a, b, apples, oranges))
# 1
# 2

s, t, a, b, apples, oranges = 7, 11, 5, 15, [-2, 2, 1], [5, -6]
print(count_apples_and_oranges(s, t, a, b, apples, oranges))
# 1
# 1
