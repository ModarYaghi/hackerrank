"""Hacker Rank Problem Solving - Challenge 45; Picking Numbers"""


def picking_numbers(a):
    subarray = []

    for i in a:
        # test_arrays will contain two nested arrays;
        # one for values of equal or greater by 1 to i,
        # the other for values of equal or smaller by 1 to i.
        test_arrays = [
            [j for j in a if j == i or j - i == 1],
            [j for j in a if j == i or i - j == 1],
        ]
        # saving array of greater length to subarray:
        for array in test_arrays:
            if len(array) > len(subarray):
                subarray = array

    return len(subarray)


# The primary test instances:
a = [1, 1, 2, 2, 4, 4, 5, 5, 5]  # [4,4,5,5,5], 5
print(picking_numbers(a))

a = [4, 6, 5, 3, 3, 1]  # [4,3,3], 3
print(picking_numbers(a))

a = [1, 2, 2, 3, 1, 2]  # [1,2,2,1,2], 5
print(picking_numbers(a))
