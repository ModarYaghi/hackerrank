"""Practice > Algorithms > String > Hackerrank in a String!"""


def hackerrank_in_string(s):

    key = "hackerrank"
    i = 0

    for char in s:
        if char == key[i]:
            i += 1
            if i == len(key):
                return "YES"

    return "NO"


# The Primary Tests' instances:
s = "haacckkerrannkk"
print(hackerrank_in_string(s))
# Yse
s = "hccaakkerankk"
print(hackerrank_in_string(s))
# No
s = "hccaakkerrannkk"
print(hackerrank_in_string(s))
# No
s = "hereiamstackerrank"
print(hackerrank_in_string(s))
# Yes
s = "hackerworld"
print(hackerrank_in_string(s))
# No
s = "hhaacckkekraraannk"
print(hackerrank_in_string(s))
# Yes
s = "rhbaasdndfsdskgbfefdbrsdfhuyatrjtcrtyytktjjt"
print(hackerrank_in_string(s))
# No
