"""Hacker Rank Problem Solving - Challenge 45; Picking Numbers, Naive approach"""


def picking_numbers(a):
    subarray = []

    for i in a:
        plus_array, minus_array = [], []
        # plus_array will contain values of equal or greater by 1 to i.
        # minus_array will contain values of equal or smaller by 1 to i.

        for j in a:
            if j == i:
                plus_array.append(j)
                minus_array.append(j)
            elif j - i == 1:
                plus_array.append(j)
            elif i - j == 1:
                minus_array.append(j)

        # saving array of greater length to subarray.
        if len(plus_array) > len(subarray):
            subarray = plus_array
        elif len(minus_array) > len(subarray):
            subarray = minus_array

    return len(subarray)


# The primary test instances:
a = [1, 1, 2, 2, 4, 4, 5, 5, 5]  # [4,4,5,5,5], 5
print(picking_numbers(a))
a = [4, 6, 5, 3, 3, 1]  # [4,3,3], 3
print(picking_numbers(a))
a = [1, 2, 2, 3, 1, 2]  # [1,2,2,1,2], 5
print(picking_numbers(a))
