# Hacker Rank Problem Solving - Challenge 46; Climbing Leaderboard

## The Challenge Statement:
*[Hacker Rank](https://www.hackerrank.com/dashboard)


### Three Python Solutions

#### 1. [Climbing Leaderboard - Solution 0](https://gitlab.com/ModarYaghi/hackerrank/-/blob/master/Climbing_Leaderboard/climbing_leaderboard_0.py)
This solution was my first try, and it has passed the primary tests' instances.
But it failed with the submitting tests, as it seems isn't efficient dealing with time complexity when the input is larger enough.


#### 2. [Climbing Leaderboard - Solution 1](https://gitlab.com/ModarYaghi/hackerrank/-/blob/master/Climbing_Leaderboard/climbing_leaderboard_1.py)
   
By using the binary search and Insertion algorithm, I could overcome the time complexity issue.
This bunch of code has passed all the tests successfully.  
But they seem kind of long and complicated for just solving this simple challenge!


#### 3. [Climbing Leaderboard - Solution 2](https://gitlab.com/ModarYaghi/hackerrank/-/blob/master/Climbing_Leaderboard/climbing_leaderboard_2.py)

After Solution 1 has passed the tests, I found this solution (Solution 2) on [the editorial page of the challenge](https://www.hackerrank.com/challenges/climbing-the-leaderboard/editorial), along with solutions for other languages, developed by [Stefan K](https://www.hackerrank.com/profile/StefanK).

IT IS BEAUTIFUL
