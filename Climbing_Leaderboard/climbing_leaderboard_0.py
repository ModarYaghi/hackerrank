"""Hacker Rank Problem Solving - Challenge 46; Climbing Leaderboard"""


def climbing_leaderboard(ranked, player):

    player_ranks = []

    for rank in player:
        ranked.append(rank)
        ranked = sorted(
            [i for j, i in enumerate(ranked) if i not in ranked[:j]], reverse=True
        )
        player_ranks.append(ranked.index(rank) + 1)

    return player_ranks


# The primary test instances:
ranked, player = [100, 90, 90, 80], [70, 80, 105]
print(climbing_leaderboard(ranked, player))
# [4, 3, 1]

ranked, player = [100, 100, 50, 40, 40, 20, 10], [5, 25, 50, 120]
print(climbing_leaderboard(ranked, player))
# [6, 4, 2, 1]

ranked, player = [100, 90, 90, 80, 75, 60], [50, 65, 77, 90, 102]
print(climbing_leaderboard(ranked, player))
# [6, 5, 4, 2, 1]
