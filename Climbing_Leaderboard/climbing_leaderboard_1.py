"""Hacker Rank Problem Solving - Challenge 46; Climbing Leaderboard"""


def binary_search(seq, key):
    begin_idx = 0
    end_idx = len(seq) - 1

    while begin_idx <= end_idx:
        midpoint = begin_idx + (end_idx - begin_idx) // 2

        if key == seq[midpoint]:
            return midpoint
        elif key > seq[midpoint] and key < seq[midpoint - 1]:
            return midpoint
        elif key < seq[midpoint] and key >= seq[midpoint + 1]:
            return midpoint + 1
        else:
            if key > seq[midpoint]:
                end_idx = midpoint - 1
            else:
                begin_idx = midpoint + 1
    return -1


def climbing_leaderboard(ranked, player):
    player_ranks = []
    ranks = [1]

    for i in range(1, len(ranked)):
        if ranked[i] == ranked[i - 1]:
            ranks.append(ranks[i - 1])
        else:
            ranks.append(ranks[i - 1] + 1)

    for j in range(len(player)):
        player_score = player[j]

        if player_score > ranked[0]:
            player_ranks.append(1)

        elif player_score < ranked[len(ranked) - 1]:
            player_ranks.append(ranks[len(ranked) - 1] + 1)

        else:
            idx = binary_search(ranked, player_score)
            player_ranks.append(ranks[idx])

    return player_ranks


# The primary test's instances:
ranked, player = [100, 90, 90, 80], [70, 80, 105]
print(climbing_leaderboard(ranked, player))
# [4, 3, 1]

ranked, player = [100, 100, 50, 40, 40, 20, 10], [5, 25, 50, 120]
print(climbing_leaderboard(ranked, player))
# [6, 4, 2, 1]

ranked, player = [100, 90, 90, 80, 75, 60], [50, 65, 77, 90, 102]
print(climbing_leaderboard(ranked, player))
# [6, 5, 4, 2, 1]
