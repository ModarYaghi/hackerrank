"""
Hacker Rank Problem Solving - Challenge 46; Climbing Leaderboard
I got this solution from view editorial of the challenge
"""


def climbing_leaderboard(ranked, player):

    # get a list of unique scores, still sorted descending
    scores = []
    i = 0
    lr = len(ranked)
    while i < lr:
        uniq_score = ranked[i]
        scores.append(uniq_score)
        while i < lr and uniq_score == ranked[i]:
            i += 1

    ls = len(scores)
    player_ranks = []

    # initialize pointer outside the loop
    # the scores are sorted, so this prevents
    # iterating lower scores again
    i = 0

    # the index provides the ranking (n - i + 1)
    for score in player:
        # work from right to left in scores array
        while i < ls and scores[ls - i - 1] <= score:
            i += 1

        player_ranks.append(ls - i + 1)

    return player_ranks


# The primary test instances:
ranked, player = [100, 90, 90, 80], [70, 80, 105]
print(climbing_leaderboard(ranked, player))
# [4, 3, 1]

ranked, player = [100, 100, 50, 40, 40, 20, 10], [5, 25, 50, 120]
print(climbing_leaderboard(ranked, player))
# [6, 4, 2, 1]

ranked, player = [100, 90, 90, 80, 75, 60], [50, 65, 77, 90, 102]
print(climbing_leaderboard(ranked, player))
# [6, 5, 4, 2, 1]
