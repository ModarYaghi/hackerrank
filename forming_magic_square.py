"""Hacker Rank Problem Solving - Challenge 44; Formaing a Magic Square"""
import itertools


def forming_magic_square(s):

    # Flaten s. Also it can be made by: s = sum(s, [])
    s = list(itertools.chain.from_iterable(s))

    # All possible distibutions that can be a magic square:
    magic_squares = [
        [8, 1, 6, 3, 5, 7, 4, 9, 2],
        [6, 1, 8, 7, 5, 3, 2, 9, 4],
        [4, 9, 2, 3, 5, 7, 8, 1, 6],
        [2, 9, 4, 7, 5, 3, 6, 1, 8],
        [8, 3, 4, 1, 5, 9, 6, 7, 2],
        [4, 3, 8, 9, 5, 1, 2, 7, 6],
        [6, 7, 2, 1, 5, 9, 8, 3, 4],
        [2, 7, 6, 9, 5, 1, 4, 3, 8],
    ]

    # All possible cost by which the input square can be turned to a magic square:
    costs = []
    # Looping through possible magic squares' instences:
    # by a list comprehension; making a list of the absolute difference between
    # each item of each instence of the possible magic squares and each item of
    # the input square.
    # Getting the sum of the list of the absolute differences.
    # Then, append them into the costs list.
    for magic_square in magic_squares:
        costs.append(sum([abs(magic_square[i] - s[i]) for i in range(9)]))

    # Returning the lowest value in the costs list.
    return min(costs)


# Test Instances:
s = [[5, 3, 4], [1, 5, 8], [6, 4, 2]]
print(forming_magic_square(s))  # 7

s = [[4, 9, 2], [3, 5, 7], [8, 1, 5]]
print(forming_magic_square(s))  # 1

s = [[4, 8, 2], [4, 5, 7], [6, 1, 6]]
print(forming_magic_square(s))  # 4

s = [[4, 4, 6], [8, 5, 1], [2, 7, 6]]
print(forming_magic_square(s))  # 4
