"""Hacker Rank Problem Solving - Challenge 14; Between Two Sets

Though this problem can be solved with a brute-force approach,
there is a faster, easier way!

Observation:
    - All numbers in A evenly divde x if and only if x is divisible by
        the Least Common Multiple (LCM) of all numbers in A. Let's
        donate the LCM of A as (factor)
    - x evenly divides all numbers in B if and only if x divides
        the Greatest Common Divisor (GCD) of all numbers in B.
        Let's donate GCD of B as (multiple)

Approach:
    Let's find the number of x values satisfying (x mod factor = 0) and
    (multiple mod x = 0).

    - If (multiple) is not divisible by (factor), no such x.
    - if some x exists, we can divde multiple, x, factor by factor.
        Now we just need to find the number of divisors of (multiple / factor),
        which we can do in (O(root of C)) time (or faster), where C is the
        maximum number in sets A and B.

Konstantin Semenov, HackerRank@zemen
***
Note: Mr. Konstantin Semenov presented solutions for this challenge in
C++ and Java. Here, I just interpreted the same approach to Python.
***
"""
from functools import reduce


def gcd(a, b):
    """Greatest Common Divisor"""
    while a % b != 0:
        a, b = b, a % b
    return b


def lcm(a, b):
    """Least Common Multiple"""
    return a * b // gcd(a, b)


def get_totalX(a, b):
    factor = reduce(lcm, a)
    multiple = reduce(gcd, b)

    totalX = 0
    for i in range(factor, multiple + 1, factor):
        if i % factor == 0 and multiple % i == 0:
            totalX += 1

    return totalX


# The primary tests' instances:
a, b = [2, 6], [24, 36]
print(get_totalX(a, b))
# [6, 12]: 2

a, b = [2, 4], [16, 32, 96]
print(get_totalX(a, b))
# [4, 8, 16]: 3

a, b = [3, 4], [24, 48]
print(get_totalX(a, b))
# [12, 24]: 2
