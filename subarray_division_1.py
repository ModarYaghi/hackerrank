"""Hacker Rank Problem Solving - Challenge 16; Sub-Array Division."""


def birthday(s, d, m):

    counter = 0

    for i in range(len(s)):
        if sum(s[i : i + m]) == d:
            counter += 1
    return counter


# The Primary Test Instances:
s, d, m = [2, 2, 1, 3, 2], 4, 2
print(birthday(s, d, m))  # 2

s, d, m = [1, 2, 1, 3, 2], 3, 2
print(birthday(s, d, m))  # 2

s, d, m = [1, 1, 1, 1, 1, 1], 3, 2
print(birthday(s, d, m))  # 0

s, d, m = [4], 4, 1
print(birthday(s, d, m))  # 1

# Testing a special instance:
s, d, m = [1, 1, 1, 1, 1, 1], 1, 1
print(birthday(s, d, m))  # 6
